function validarEmail(email){
    if(email.indexOf("@")!=-1){
        var user = email.substring(0, email.indexOf("@"));
        var dominio = email.substring(email.indexOf("@")+1, email.length);
        if(email.indexOf(" ")<0){
            if(user != "" && dominio != ""){
                if(dominio.indexOf(".")==-1){
                    return false;
                }else{
                    return true;
                }
            }else{
                return false;
            }
        }else{
            return false;
        }
    }else{
        return false;
    }
}
/*
    Não possuir espaços;
    Possuir o @;
    Possuir algum caracter após o @;
    Possuir algum caracter antes do @;
    Possuir pelo menos um ponto após o caracter depois do @;
    Possuir algum caracter após o ponto.
*/
$(document).ready(()=>{
    var dados = sessionStorage.getItem("dados-front");
    if( dados != undefined &&
        dados != "undefined" &&
        dados != null &&
        dados != "null" &&
        dados != "" &&
        dados != 0 &&
        dados != [] &&
        dados != " "){
        dados = dados.split(",");
        $("#nome").val(dados[0]);
        $("#email").val(dados[1]);
        $("#telefone").val(dados[2]);
        $("#assunto").val(dados[3]);
        $("#mensagem").val(dados[4]);

        $("#vNome").text(dados[0]);
        $("#vEmail").text(dados[1]);
        $("#vTelefone").text(dados[2]);
        $("#vAssunto").text(dados[3]);
        $("#vMensagem").text(dados[4]);
    }
    $("#nome").change(()=>{
        $("#vNome").text("Valor: "+$("#nome").val());
    });
    $("#email").change(()=>{
        $("#vEmail").text("Valor: "+$("#email").val());
    });
    $("#telefone").change(()=>{
        $("#vTelefone").text("Valor: "+$("#telefone").val());
    });
    $("#assunto").change(()=>{
        $("#vAssunto").text("Valor: "+$("#assunto").val());
    });
    $("#mensagem").change(()=>{
        $("#vMensagem").text("Valor: "+$("#mensagem").val());
    });
    $("#nome").keypress(()=>{
        $("#vNome").text("Valor: "+$("#nome").val());
    });
    $("#email").keypress(()=>{
        $("#vEmail").text("Valor: "+$("#email").val());
    });
    $("#telefone").keypress(()=>{
        $("#vTelefone").text("Valor: "+$("#telefone").val());
    });
    $("#assunto").keypress(()=>{
        $("#vAssunto").text("Valor: "+$("#assunto").val());
    });
    $("#mensagem").keypress(()=>{
        $("#vMensagem").text("Valor: "+$("#mensagem").val());
    });
    $("#submitContact").on("click", ()=>{
        if($("#nome").val()!='' && $("#email").val()!='' && $("#telefone").val()!='' && $("#assunto").val()!='' && $("#mensagem").val()!=''){
            if(validarEmail($("#email").val())){
                var dados = [];
                dados[0]=$("#nome").val();
                dados[1]=$("#email").val();
                dados[2]=$("#telefone").val();
                dados[3]=$("#assunto").val();
                dados[4]=$("#mensagem").val();
                sessionStorage.setItem("dados-front", dados);
                swal({
                    title: "Salvo com sucesso",
                    text: "Você salvou suas informações para exclui-las aperte no botão de limpar",
                    icon: "success",
                });
            }else{
                swal({
                    title: "E-mail inválido",
                    text: "O campo E-mail está preenchido de forma incorreta",
                    icon: "erro",
                });
            }
        }else{
            swal({
                title: "Campos vazios",
                text: "Os campos estão vazios, favpr preenche-los",
                icon: "erro",
            });
        }
    });
    $("#resetContact").on("click", ()=>{
        sessionStorage.setItem("dados-front", null);
        $("#vNome").text("");
        $("#vTelefone").text("");
        $("#vEmail").text("");
        $("#vAssunto").text("");
        $("#vMensagem").text("");
        swal({
            title: "Limpo com sucesso",
            text: "Seus dados foram excluidos com sucesso",
            icon: "success",
        });
    });
});